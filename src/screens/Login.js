import React, { Component } from "react";
import {
  Text,
  View,
  TextInput,
  Button,
  StyleSheet,
  Dimensions,
  TouchableOpacity
} from "react-native";
import { connect } from "react-redux";

import { getLogin } from "../actions/LoginAction";

class LoginSimple extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: ""
    };
  }
  handleUsername = username => {
    // console.log(username);
    this.setState({
      username: username
    });
  };

  handlePassword = password => {
    // console.log(password);
    this.setState({
      password: password
    });
  };

  handleButton = () => {
    this.props.dispatch(
      getLogin({
        username: this.state.username,
        password: this.state.password
      })
    );
  };

  handleRegister = () => {
    this.props.navigation.navigate("RegisterScreen");
  };
  render() {
    return (
      <View style={styles.container}>
        <TextInput
          style={styles.input}
          placeholder="Username"
          onChangeText={this.handleUsername}
          value={this.state.username}
        />
        <TextInput
          style={styles.input}
          placeholder="Password"
          //   returnKeyLabel="password"
          onChangeText={this.handlePassword}
          value={this.state.password}
        />
        <TouchableOpacity onPress={this.handleRegister}>
          <Text style={{ marginTop: 20 }}>Register</Text>
        </TouchableOpacity>

        <Button
          style={styles.btn}
          onPress={this.handleButton}
          color={"red"}
          title="Login"
        />
      </View>
    );
  }
}

const DEVICE_WIDTH = Dimensions.get("window").width;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff"
  },
  input: {
    backgroundColor: "#e5e6e8",
    width: DEVICE_WIDTH - 40,
    height: 40,
    marginTop: 10,
    marginHorizontal: 20,
    paddingLeft: 45,
    borderRadius: 20,
    color: "#000"
  }
});

const mapStateToProps = state => {
  return {
    ...state.loginUser
  };
};

export default connect(mapStateToProps)(LoginSimple);

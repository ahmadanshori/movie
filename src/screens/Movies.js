import React, { Component } from "react";
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  ScrollView,
  View,
  StatusBar,
  ActivityIndicator
} from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import Swiper from "react-native-swiper";
import { connect } from "react-redux";

// import SwiperImg from "../components/Swiper/Swiper";
import Container from "../components/Container/Container";
import ButtonMovie from "../components/Button/ButtonMovies";
import MovieCard from "../components/MovieCard/MovieCard";
import MovieSlider from "../components/MovieSlider/MovieSlider";
import { getNowPlaying, getPopular } from "../actions/Movies";

class Movies extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  static navigationOptions = ({ navigation }) => ({
    title: "Movies",
    headerLeft: (
      <TouchableOpacity
        style={styles.icon}
        onPress={() => navigation.openDrawer()}
      >
        <Icon name="ios-menu" size={30} color="#ffffff" />
      </TouchableOpacity>
    ),
    headerStyle: {
      backgroundColor: "#000000"
    },
    headerTintColor: "white"
  });

  componentDidMount() {
    this.props.dispatch(getNowPlaying());
    this.props.dispatch(getPopular());
  }

  handleMovieSeeAll = () => {
    this.props.navigation.navigate("MovieList", {
      title: "popular",
      category: "popular"
    });
  };
  handleMovieNowPlaying = () => {
    this.props.navigation.navigate("MovieList", {
      title: "Now Playing",
      category: "now_playing"
    });
  };
  handleMoviesUpcoming = () => {
    this.props.navigation.navigate("MovieList", {
      title: "Upcoming",
      category: "upcoming"
    });
  };
  handleTopRated = () => {
    this.props.navigation.navigate("MovieList", {
      title: "Top Rated",
      category: "top_rated"
    });
  };
  handleMovieDetail = movie => {
    this.props.navigation.navigate("MovieDetails", {
      id: movie.id
    });
  };

  render() {
    return (
      <Container>
        <StatusBar backgroundColor="black" barStyle="light-content" />
        {!this.props.nowPlaying.results.length ||
        !this.props.popular.results.length ? (
          <ActivityIndicator
            style={{ flex: 1, alignSelf: "center" }}
            size={"large"}
            color={"red"}
          />
        ) : (
          <ScrollView style={styles.container}>
            <Swiper
              height={250}
              autoplay
              autoplayTimeout={4}
              loop
              horizontal
              showsPagination={false}
            >
              {this.props.nowPlaying.results.map(movie => {
                return (
                  <MovieSlider
                    key={movie.id}
                    titleMovie={movie.original_title}
                    genre="Action"
                    rating={movie.vote_average}
                    desc={movie.overview}
                    wrappe={{ uri: `${movie.poster_path}` }}
                    img={{ uri: `${movie.poster_path}` }}
                    buttonPress={() => this.handleMovieDetail(movie)}
                  />
                );
              })}
            </Swiper>

            <View>
              <View style={styles.listHeading}>
                <Text style={styles.listHeadingLeft}>Popular</Text>
                <TouchableOpacity onPress={this.handleMovieSeeAll}>
                  <Text style={styles.listHeadingRight}>See all</Text>
                </TouchableOpacity>
              </View>
            </View>

            <ScrollView horizontal showsHorizontalScrollIndicator={false}>
              {this.props.popular.results.map(movie => (
                <MovieCard
                  key={movie.id}
                  titleMovie={movie.original_title}
                  img={{ uri: `${movie.poster_path}` }}
                  onPress={() => this.handleMovieDetail(movie)}
                />
              ))}
            </ScrollView>

            <View>
              <ButtonMovie
                buttonIcon="md-play"
                text="Now Playing"
                onPress={this.handleMovieNowPlaying}
              />
              <ButtonMovie
                buttonIcon="md-trending-up"
                text="Top Rated"
                onPress={this.handleTopRated}
              />
              <ButtonMovie
                buttonIcon="md-recording"
                text="Upcoming"
                onPress={this.handleMoviesUpcoming}
              />
            </View>
          </ScrollView>
        )}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  icon: {
    paddingLeft: 10
  },
  drawer: {
    flex: 1
  },
  listHeading: {
    paddingHorizontal: 16,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 15,
    marginTop: 30
  },
  listHeadingLeft: {
    color: "white",
    fontWeight: "bold",
    fontSize: 18
  },
  listHeadingRight: {
    color: "white",
    fontSize: 16
  }
});

const mapStateToProps = state => {
  return {
    nowPlaying: state.nowPlaying,
    popular: state.popular
  };
};

export default connect(mapStateToProps)(Movies);

// export default Movies;

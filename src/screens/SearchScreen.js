import React, { Component } from "react";
import {
  StyleSheet,
  TouchableOpacity,
  TextInput,
  FlatList,
  ActivityIndicator,
  View,
  StatusBar
} from "react-native";
import { connect } from "react-redux";
import Icon from "react-native-vector-icons/Ionicons";
import Container from "../components/Container/Container";
import MovieList from "../components/MovieList/MovieList";
import { searchMovie, clearMovieList } from "../actions/Movies";
import { TMBD_IMG } from "../config/API";
class SearchScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      query: "",
      page: 1
    };
  }
  static navigationOptions = ({ navigation }) => {
    return {
      title: "Search",
      headerLeft: (
        <TouchableOpacity
          style={styles.icon}
          onPress={() => navigation.navigate("Movies")}
        >
          <Icon name="ios-arrow-round-back" size={30} color="#fff" />
        </TouchableOpacity>
      ),
      headerStyle: {
        backgroundColor: "#000000"
      },
      headerTintColor: "white"
    };
  };
  movieDetail = movie => {
    // console.log("aaaaaaa", movie);
    // this.props.dispatch(clearMovieList());
    this.props.navigation.navigate("MovieDetails", {
      id: movie.id
    });
  };
  handleChangeText = text => {
    this.setState({
      query: text
    });
  };
  handleOnSubmit = () => {
    this.props.dispatch(searchMovie(this.state.query));
  };
  nextPage = () => {
    this.setState(
      {
        page: this.state.page + 1
      },
      () => {
        this.props.dispatch(searchMovie(this.state.query));
      }
    );
  };
  renderItem = ({ item }) => {
    return (
      <MovieList
        movieId={item.id}
        title={item.original_title}
        img={{ uri: `${TMBD_IMG}${item.poster_path}` }}
        rating={item.vote_average}
        year={new Date(item.release_date).getFullYear()}
        desc={item.overview}
        onPress={() => this.movieDetail(item)}
      />
    );
  };
  render() {
    // console.log(this.props.searchList);
    return (
      <Container>
        <StatusBar backgroundColor="black" barStyle="light-content" />
        {!this.props.searchList ? (
          <ActivityIndicator
            style={{ flex: 1, alignSelf: "center" }}
            size={"large"}
            color={"red"}
          />
        ) : (
          <View>
            <TextInput
              style={styles.input}
              autoFocus
              onSubmitEditing={this.handleOnSubmit}
              onChangeText={this.handleChangeText}
              placeholder="Cari Disini Bro !!!"
              value={this.state.query}
            />
            <FlatList
              onEndReached={this.nextPage}
              onEndReachedThreshold={0.5}
              data={this.props.searchList.results || []}
              renderItem={this.renderItem}
              keyExtractor={item => item.id.toString()}
            />
          </View>
        )}
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  IconStyle: {
    marginLeft: 8
  },
  input: {
    backgroundColor: "white",
    height: 40,
    margin: 16,
    borderRadius: 3
  },
  icon: {
    marginLeft: 13
  }
});
const mapStateToProps = state => ({
  searchList: state.searchList
});
export default connect(mapStateToProps)(SearchScreen);

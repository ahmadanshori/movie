import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
  StyleSheet,
  Image,
  Dimensions,
  ActivityIndicator,
  StatusBar,
  Platform
} from "react-native";
import { connect } from "react-redux";
import Icon from "react-native-vector-icons/Ionicons";
import Swiper from "react-native-swiper";
import { TabView, SceneMap } from "react-native-tab-view";
import moment from "moment";

import { TabCast, TabInfo, TabTrailers } from "../components/Tab";
import Container from "../components/Container/Container";
import MovieDetail from "../components/MovieDetail/MovieDetail";
import { getDetail } from "../actions/Movies";
import { TMBD_IMG } from "../config/API";
// import Swiper from "../components/Swiper/Swiper";

// const {movieDetail} = this.props;
const InfoRoute = props => {
  const { movieDetail } = props;

  const director = movieDetail.casts.crew.find(crew => {
    return crew.job == "Director";
  });

  return (
    <TabInfo
      tabLabel="INFO"
      overview={movieDetail.overview}
      budget={movieDetail.budget}
      directedBy={director.name}
      releaseDate={moment(movieDetail.release_date).format("DD MMMM YYYY")}
    />
  );
};
const CastsRoute = props => {
  const { movieDetail } = props;
  return <TabCast tabLabel="CASTS" detail={movieDetail} />;
};

const TrailersRoute = props => {
  const { movieDetail } = props;
  return <TabTrailers tabLabel="TRAILERS" detail={movieDetail} />;
};

class MovieDetails extends Component {
  constructor(props) {
    super(props);

    this.state = {
      index: 0,
      routes: [
        { key: "info", title: "INFO" },
        { key: "casts", title: "CASTS" },
        { key: "trailers", title: "TRAILERS" }
      ]
    };
  }

  static navigationOptions = ({ navigation }) => ({
    header: null
  });

  componentDidMount() {
    this.props.dispatch(getDetail(this.props.navigation.state.params.id));
  }

  handleButtonClose = () => {
    this.props.navigation.goBack();
  };

  render() {
    const { movieDetail } = this.props;
    if (!movieDetail.id) {
      return (
        <ActivityIndicator style={{ flex: 1 }} color={"#EA0000"} size="large" />
      );
    }

    // console.log("*****", this.props.movieDetail.genres);
    // console.log(movieDetail.images.backdrops);
    const genre = movieDetail.genres.map(item => {
      return item.name;
    });
    let dataGenre = genre.join();

    return (
      <Container>
        <StatusBar backgroundColor="black" barStyle="light-content" />
        <TouchableOpacity
          style={styles.buttonClose}
          onPress={this.handleButtonClose}
        >
          <Icon
            name="md-close"
            size={36}
            color={"red"}
            style={styles.iconClose}
          />
        </TouchableOpacity>

        <View style={{ height: 350 }}>
          <Swiper autoplay autoplayTimeout={3} showsPagination={false}>
            {movieDetail.images.backdrops.map(item => {
              return (
                <Image
                  key={item.file_path}
                  style={{ flex: 1, opacity: 0.5 }}
                  source={{
                    uri: `${TMBD_IMG}${item.file_path}`
                  }}
                />
              );
            })}
          </Swiper>
          <View>
            <MovieDetail
              img={{ uri: `${TMBD_IMG}${movieDetail.poster_path}` }}
              title={movieDetail.original_title}
              tagLine={movieDetail.tagline}
              rating={movieDetail.vote_average}
              genre={dataGenre}
            />
          </View>
        </View>

        <TabView
          navigationState={this.state}
          renderScene={SceneMap({
            info: () => <InfoRoute movieDetail={this.props.movieDetail} />,
            casts: () => <CastsRoute movieDetail={this.props.movieDetail} />,
            trailers: () => (
              <TrailersRoute movieDetail={this.props.movieDetail} />
            )
          })}
          onIndexChange={index => this.setState({ index })}
          initialLayout={{
            width: Dimensions.get("window").width,
            height: 50
          }}
        />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  buttonClose: {
    position: "absolute",
    ...Platform.select({
      ios: {
        marginTop: 40
      }
    }),
    left: 0,
    top: 0,
    marginLeft: 10,
    zIndex: 999
  },
  icon: {
    paddingLeft: 10
  },
  iconClose: {
    padding: 10
  },
  scene: {
    flex: 1
  }
});

const mapStateToProps = state => {
  return {
    movieDetail: state.movieDetail.data || {}
  };
};

export default connect(mapStateToProps)(MovieDetails);

import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  StatusBar,
  ActivityIndicator
} from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import { connect } from "react-redux";

import Container from "../components/Container/Container";
import MovieList from "../components/MovieList/MovieList";
import { getList, clearMovieList } from "../actions/Movies";
import { TMBD_IMG } from "../config/API";

// import MovieVertical from "../components/MovieVertical/MovieVertical";
class MovieListScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: navigation.state.params.title,
    headerLeft: (
      <TouchableOpacity style={styles.icon} onPress={() => navigation.goBack()}>
        <Icon name="ios-arrow-round-back" size={35} color="#fff" />
      </TouchableOpacity>
    ),
    headerStyle: {
      backgroundColor: "#000000"
    },
    headerTintColor: "white"
  });

  constructor(props) {
    super(props);
    this.state = {
      page: 1
    };
  }

  componentDidMount() {
    this.props.dispatch(
      getList({
        category: this.props.navigation.state.params.category,
        page: this.state.page
      })
    );
  }

  componentWillUnmount() {
    this.props.dispatch(clearMovieList());
  }

  handleMovieList = movie => {
    this.props.navigation.navigate("MovieDetails", {
      id: movie.id
    });
  };

  renderItem = ({ item }) => {
    return (
      <MovieList
        movieId={item.id}
        title={item.original_title}
        rating={item.vote_average}
        img={{ uri: `${TMBD_IMG}${item.poster_path}` }}
        year={new Date(item.release_date).getFullYear()}
        desc={item.overview}
        onPress={() => this.handleMovieList(item)}
      />
    );
  };
  newPage = () => {
    this.props.dispatch(
      getList({
        category: this.props.navigation.state.params.category,
        page: ++this.state.page
      })
    );
  };

  render() {
    return (
      <Container>
        <StatusBar backgroundColor="black" barStyle="light-content" />
        {!this.props.movieList.results ? (
          <ActivityIndicator
            style={{ flex: 1, alignSelf: "center" }}
            size={"large"}
            color={"red"}
          />
        ) : (
          <FlatList
            onEndReached={this.newPage}
            onEndReachedThreshold={0.5}
            data={this.props.movieList.results}
            keyExtractor={item => item.id.toString()}
            renderItem={this.renderItem}
          />
        )}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  icon: {
    paddingLeft: 15
  }
});

const mapStateToProps = state => {
  return {
    movieList: state.movieList
  };
};

export default connect(mapStateToProps)(MovieListScreen);

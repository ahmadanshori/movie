import { put, takeEvery, call, takeLatest } from "redux-saga/effects";
const axios = require("axios");
import {
  GET_POPULAR_MOVIES,
  GET_POPULAR_MOVIES_SUCCESS,
  GET_POPULAR_MOVIES_FAILED,
  GET_NOWPLAYING_MOVIES,
  GET_NOWPLAYING_MOVIES_SUCCESS,
  GET_NOWPLAYING_MOVIES_FAILED,
  GET_MOVIES_LIST,
  GET_MOVIES_LIST_SUCCESS,
  GET_MOVIES_LIST_FAILED,
  GET_MOVIES_DETAIL,
  GET_MOVIES_DETAIL_SUCCESS,
  GET_MOVIES_DETAIL_FAILED,
  SEARCH_MOVIES,
  SEARCH_MOVIES_SUCCESS,
  SEARCH_MOVIES_FAILED
} from "../actions/types";

import { TMDB_HOST, TMBD_KEY, TMBD_IMG } from "../config/API";
// const TMDB_HOST = "https://api.themoviedb.org/3";
// const TMBD_KEY = "128f9d885d9a099a5bcfbf499a939ffd";
// const TMBD_IMG = "https://image.tmdb.org/t/p/w500";

////////////////  Fetch API  ///////////////////////
export const getAPI = ({ category, page = 1 }) => {
  return fetch(
    `${TMDB_HOST}/movie/${category}?api_key=${TMBD_KEY}&page=${page}`
  );
};
export const searchAPI = data => {
  //   console.log(query);
  return fetch(
    `${TMDB_HOST}/search/movie?api_key=${TMBD_KEY}&query=${
      data.query
    }&append_to_response=videos,images`
  );
};
export const detailAPI = id => {
  //   console.log(
  //     `${TMDB_HOST}/movie/${id}?api_key=${TMBD_KEY}&append_to_response=videos,images,casts`
  //   );
  return fetch(
    `${TMDB_HOST}/movie/${id}?api_key=${TMBD_KEY}&append_to_response=videos,images,casts`
  );
};

///////////////  End Fetch API  //////////////////

////////////// Axios ////////////////////////////
// export const getAPI = ({ category, page = 1 }) => {
//   return axios.get(
//     `${TMDB_HOST}/movie/${category}?api_key=${TMBD_KEY}&page=${page}`
//   );
// };

// export const searchAPI = data => {
//   return axios.get(
//     `${TMDB_HOST}/search/movie?api_key=${TMBD_KEY}&query=${
//       data.query
//     }&append_to_response=videos,images`
//   );
// };

// export const detailAPI = id => {
//   return axios.get(
//     `${TMDB_HOST}/movie/${id}?api_key=${TMBD_KEY}&append_to_response=videos,images,casts`
//   );
// };
////////////// End Axios ///////////////////////

function* getListSaga(action) {
  try {
    const response = yield getAPI({
      category: action.category,
      page: action.page
    });
    const result = yield response.json();
    // console.log(result);
    // let data = result.results;
    // const data = {
    //   ...result,
    //   poster: result.results.map(item => {
    //     return `${TMBD_IMG}${item.poster_path}`;
    //   })
    // };

    yield put({
      type: GET_MOVIES_LIST_SUCCESS,
      data: result
    });
  } catch (err) {
    yield put({
      type: GET_MOVIES_LIST_FAILED,
      error: err.response ? err.response.data.message : err.message
    });
  }
}

function* getPopularSaga(action) {
  try {
    const response = yield getAPI({
      category: "popular"
    });
    const result = yield response.json();
    let data = result.results;

    data.map(item => {
      item.poster_path = `${TMBD_IMG}${item.poster_path}`;
      return item;
    });
    yield put({
      type: GET_POPULAR_MOVIES_SUCCESS,
      data: data
    });
  } catch (err) {
    yield put({
      type: GET_POPULAR_MOVIES_FAILED,
      error: err.response ? err.response.data.message : err.message
    });
  }
}

function* getNowPlayingSaga(action) {
  try {
    const response = yield getAPI({
      category: "now_playing"
    });
    const result = yield response.json();
    let data = result.results;

    data.map(function(item) {
      item.poster_path = `${TMBD_IMG}${item.poster_path}`;
      return item;
    });
    // console.log(data);
    yield put({
      type: GET_NOWPLAYING_MOVIES_SUCCESS,
      data: data
    });
  } catch (err) {
    yield put({
      type: GET_NOWPLAYING_MOVIES_FAILED,
      error: err.response ? err.response.data.message : err.message
    });
  }
}

function* getDetailSaga(action) {
  try {
    const response = yield call(detailAPI, action.id);
    let result = yield response.json();
    // console.log(result);
    // const detail = {
    //   ...result,
    //   poster_path: `${TMBD_IMG}${poster_path}`,
    //   images: {
    //     ...result.image,
    //     backdrop: result.images.backdrops.map(item => {
    //       return `${TMBD_IMG}${item.file_path}`;
    //     })
    //   },
    //   casts: {
    //     ...result.casts,
    //     cast: result.casts.cast.map(item => {
    //       return `${TMBD_IMG}${item.profile_path}`;
    //     })
    //   }
    // };

    yield put({
      type: GET_MOVIES_DETAIL_SUCCESS,
      data: result
    });
  } catch (err) {
    const error = err.response ? err.response.data.message : err.message;

    yield put({
      type: GET_MOVIES_DETAIL_FAILED,
      error: error
    });
  }
}

// function* getUpcomingSaga(action) {
//   try {
//     const response = yield getAPI({
//       category: "upcoming"
//     });
//     let result = yield response.json();
//     let data = result.results;
//     data.map(function(item) {
//       item.poster_path = `${TMBD_IMG}${item.poster_path}`;
//     });
//     yield put({
//       type: GET_UPCOMING_MOVIES_SUCCESS,
//       data: data
//     });
//   } catch (err) {
//     yield put({
//       type: GET_UPCOMING_MOVIES_FAILED,
//       data: err.response ? err.response.data.message : err.message
//     });
//   }
// }

function* getSearchSaga(action) {
  try {
    const response = yield searchAPI({
      query: action.query
    });
    let result = yield response.json();
    // console.log(result);
    yield put({
      type: SEARCH_MOVIES_SUCCESS,
      data: result
    });
  } catch (err) {
    yield put({
      type: SEARCH_MOVIES_FAILED,
      error: err.response ? err.response.data.message : err.message
    });
  }
}

export default function*() {
  yield takeEvery(GET_MOVIES_LIST, getListSaga);
  yield takeLatest(GET_POPULAR_MOVIES, getPopularSaga);
  yield takeLatest(GET_NOWPLAYING_MOVIES, getNowPlayingSaga);
  yield takeEvery(GET_MOVIES_DETAIL, getDetailSaga);
  //   yield takeEvery(GET_UPCOMING_MOVIES, getUpcomingSaga);
  yield takeLatest(SEARCH_MOVIES, getSearchSaga);
}

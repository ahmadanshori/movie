import React from "react";
import {
  Image,
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  StyleSheet
} from "react-native";

import Ratings from "../Rating/index";

const MovieVertical = ({ movies }) => {
  return (
    <View style={{ flexDirection: "row" }}>
      <View style={StyleSheet.container}>
        <Image />
      </View>
      <View>
        <Text style={styles.title}>judul movies</Text>
        <Text>tanggal release</Text>
        <Ratings color="black" />
        <Text numberOfLines={3} style={styles.overview}>
          Movie Overview
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    height: 180,
    marginTop: 10,
    marginLeft: 20,
    borderTopLeftRadius: 3,
    borderBottomLeftRadius: 3
  },
  title: {
    width: 170,
    fontWeight: "bold"
  },
  overview: {
    width: 170
  }
});
export default MovieVertical;

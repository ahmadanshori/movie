import React from "react";
import { View, Text, StyleSheet } from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import PropTypes from "prop-types";

const Ratings = ({ rating, color }) => {
  let textStyle = color
    ? [styles.ratingValues, { color: color }]
    : styles.ratingValues;

  return (
    <View style={styles.container}>
      <Icon name="md-star" color="#bab523" size={20} />
      <Text style={textStyle}>{rating}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    paddingVertical: 3
  },
  ratingValues: {
    marginTop: 2,
    marginLeft: 6,
    color: "#fff"
  }
});

// Ratings.propTypes({
//   detail: PropTypes.string,
//   color: PropTypes.obj
// });

export default Ratings;

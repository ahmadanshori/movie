import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, Image } from "react-native";
import PropType from "prop-types";

const MovieCard = ({ onPress, img, titleMovie }) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.container}>
        <Image style={styles.img} source={img} />
        <View style={styles.containerText}>
          <Text style={styles.titleMovie} numberOfLines={2}>
            {titleMovie}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    width: 140,
    marginVertical: 16,
    marginRight: 16,
    borderRadius: 3
  },
  img: {
    height: 180,
    width: 140
  },
  containerText: {
    height: 40,
    paddingHorizontal: 3,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white"
  },
  titleMovie: {
    width: "100%",
    fontWeight: "bold",
    textAlign: "center"
  }
});

// MovieCard.propTypes = {
// onPress,
// img,
// titleMovie
// }

export default MovieCard;

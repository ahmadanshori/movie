import React from "react";
import {
  View,
  Image,
  ImageBackground,
  Text,
  StyleSheet,
  TouchableOpacity
} from "react-native";
import Swiper from "react-native-swiper";

import Ratings from "../Rating/index";

const SwiperImg = () => (
  <View style={{ height: 250 }}>
    <Swiper loop autoplay autoplayTimeout={4} showsPagination={false}>
      <View>
        <View style={styles.slide}>
          <Image
            resizeMode="stretch"
            style={styles.image}
            source={require("../../../assets/1.jpg")}
          />
        </View>
        {/* <View style={styles.slide}>
        <Image
          resizeMode="stretch"
          style={styles.image}
          source={require("../../../assets/2.jpg")}
        />
      </View>
      <View style={styles.slide}>
        <Image
          resizeMode="stretch"
          style={styles.image}
          source={require("../../../assets/3.jpg")}
        />
      </View>
      <View style={styles.slide}>
        <Image
          resizeMode="stretch"
          style={styles.image}
          source={require("../../../assets/4.jpg")}
        />
      </View> */}
        <View style={{ position: "absolute" }}>
          {/* <Image
        source={require("../../../assets/4.jpg")}
        style={{
          marginTop: 50,
          // resizeMode: "contain",
          marginLeft: 20,
          width: 150,
          height: 180
        }}
      /> */}
          <Text
            numberOfLines={3}
            style={{
              color: "white",
              marginTop: 50,
              fontSize: 18,
              fontWeight: "bold",
              width: 150
            }}
          >
            {/* {movie.title} */}
            aaa
          </Text>
          <Text
            style={{
              color: "white",
              marginTop: 5
            }}
          >
            Action
          </Text>
          <Ratings color={"white"} />
          <Text
            style={{
              color: "white",
              width: 170
            }}
            numberOfLines={3}
          >
            {/* {movie.overview} */}
            movie overview dnfeifnainfienfiefeifneinfefincrvrvr
            vrdwandfieanfeoanfeancveaneaine veivneinvienveivneivnevkv
            vekneveineneneinei
          </Text>
          <TouchableOpacity
            style={{
              marginTop: 5,
              padding: 2,
              paddingLeft: 3,
              backgroundColor: "red",
              width: 85,
              height: 25,
              borderRadius: 5
            }}
            onPress={() => {}}
          >
            <Text style={{ color: "white" }}>View Details</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Swiper>
  </View>
);

const styles = StyleSheet.create({
  wrapper: {},
  slide: {
    width: "100%",
    height: "100%",
    opacity: 0.5
  },
  slide2: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#97CAE5"
  },
  slide3: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#92BBD9"
  },
  text: {
    color: "#fff",
    fontSize: 30,
    fontWeight: "bold"
  }
});
export default SwiperImg;

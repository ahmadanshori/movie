import React from "react";
import {
  View,
  Image,
  ImageBackground,
  Text,
  StyleSheet,
  TouchableOpacity
} from "react-native";
import Ratings from "../Rating/index";

const MovieList = ({ movieId, onPress, img, title, year, desc, rating }) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.container}>
        <Image style={styles.image} source={img} />
        <View style={styles.containerText}>
          <Text style={styles.title}>{title}</Text>
          <Text style={styles.year}>{year}</Text>
          <Ratings rating={rating} color={"blue"} />
          <Text style={styles.desc} numberOfLines={3}>
            {desc}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    overflow: "hidden",
    marginHorizontal: 16,
    marginTop: 8,
    borderRadius: 3
  },
  image: {
    height: 160,
    width: 120
  },
  containerText: {
    flex: 1,
    padding: 8,
    backgroundColor: "#e2e2e0"
  },
  title: {
    fontWeight: "bold"
  },
  year: {
    fontWeight: "100"
  }
});

export default MovieList;

import React from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import PropTypes from "prop-types";

const ButtonMovie = ({ onPress, buttonIcon, text }) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.container}>
        <Icon name={buttonIcon} size={26} color="#417270" />
        <Text style={styles.text}>{text}</Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    width: "100%",
    flexDirection: "row",
    padding: 10
  },
  text: {
    fontSize: 18,
    marginLeft: 16,
    color: "#338480"
  }
});

// ButtonMovie.propTypes = {
//   onPress: PropTypes.func,
//   buttonIcon: PropTypes.object,
//   text: PropTypes.string
// };

export default ButtonMovie;

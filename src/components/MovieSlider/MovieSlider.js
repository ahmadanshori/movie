import React from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  ImageBackground,
  //   TouchableOpacity,
  Dimensions
} from "react-native";
// import PropTypes from "prop-types";
import AwesomeButtonRick from "react-native-really-awesome-button";

import Ratings from "../Rating/index";

const MovieSlider = ({
  wrappe,
  titleMovie,
  img,
  genre,
  rating,
  buttonPress,
  desc
}) => {
  //   console.log(rating);
  return (
    <View>
      <ImageBackground style={styles.container} source={wrappe}>
        {/* <View style={styles.containerImage}> */}
        <Image style={styles.img} source={img} />
        <View style={styles.containerText}>
          <Text style={styles.title}>{titleMovie}</Text>
          <Text style={styles.genre}>{genre}</Text>
          <Ratings rating={rating} />
          <Text style={styles.desc} numberOfLines={3}>
            {desc}
          </Text>
          <AwesomeButtonRick
            onPress={buttonPress}
            height={30}
            backgroundColor={"#990000"}
            backgroundDarker={"#800000"}
          >
            View Details
          </AwesomeButtonRick>

          {/* <TouchableOpacity style={styles.button} onPress={buttonPress}>
            <Text style={styles.buttonText}>View Details</Text>
          </TouchableOpacity> */}
        </View>
        {/* </View> */}
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 250,
    width: Dimensions.get("window").width,
    flexDirection: "row",
    backgroundColor: "#000",
    padding: 16,
    alignItems: "center"
  },
  containerImage: {},
  img: {
    width: 120,
    height: 180,
    borderRadius: 3,
    marginRight: 10
  },
  containerText: {
    width: 200,
    height: 180,
    borderRadius: 3,
    marginRight: 10
  },
  title: {
    fontWeight: "500",
    fontSize: 20,
    color: "#fff"
  },
  genre: {
    color: "#fff"
  },
  desc: {
    color: "#fff"
  }
  //   button: {
  //     height: 30,
  //     alignSelf: "baseline",
  //     padding: 5,
  //     marginTop: 10,
  //     borderRadius: 4,
  //     backgroundColor: "red"
  //   },
  //   buttonText: {
  //     color: "white"
  //   }
});

// MovieSlider.propTypes = {
//   wrappe,
//   titleMovie: PropTypes.string,
//   img,
//   genre: PropTypes.string,
//   rating: PropTypes.number,
//   buttonPress: PropTypes.func,
//   desc: PropTypes.string
// };

export default MovieSlider;

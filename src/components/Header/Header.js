import PropTypes from "prop-types";
import React from "react";
import { View, StyleSheet, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import styles from "./styles";

const Header = () => (
  <View style={styles.container}>
    <TouchableOpacity style={styles.button}>
      <Icon name="bars" size={30} color="#fff" />
    </TouchableOpacity>
  </View>
);

export default Header;

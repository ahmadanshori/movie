import React from "react";
import { View, Text, Image, StyleSheet } from "react-native";
import Ratings from "../Rating/index";
import PropTypes from "prop-types";

const MovieDetail = ({ img, title, tagLine, rating, genre }) => {
  return (
    <View style={styles.container}>
      <Image style={styles.img} source={img} />
      <View style={styles.containerTitle}>
        <Text style={styles.title}>{title}</Text>
        <Text style={styles.tagLine}>{tagLine}</Text>
        <Text style={styles.genre}>{genre}</Text>
        <Ratings rating={rating} color={"blue"} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    padding: 16,
    alignItems: "flex-end",
    height: 150
  },
  img: {
    width: 120,
    height: 180,
    borderRadius: 3,
    marginRight: 10
  },
  containerTitle: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "space-evenly"
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
    color: "#fff"
  },
  tagLine: {
    color: "#fff",
    fontSize: 20
  },
  genre: {
    color: "#fff"
  }
});

// MovieDetail.propTypes = {

// }

export default MovieDetail;

import PropTypes from "prop-types";
import React from "react";
import { View, ActivityIndicator, StyleSheet } from "react-native";

const Container = props => {
  return (
    <View style={styles.container}>
      {props.loading ? (
        <ActivityIndicator
          style={styles.Activity}
          color={"#EA0000"}
          size="large"
        />
      ) : (
        props.children
      )}
    </View>
  );
};

Container.propTypes = {
  children: PropTypes.any
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#000000"
  }
});

export default Container;

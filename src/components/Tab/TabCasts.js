import React from "react";
import {
  View,
  Text,
  Image,
  StyleSheet,
  FlatList,
  ScrollView
} from "react-native";
import { TMBD_IMG } from "../../config/API";

// const TabCast = ({ data }) => {
//   const renderItem = ({ item, img }) => {
//     return (
//       <View style={StyleSheet.itemContainer}>
//         <Image style={styles.img} source={img} />
//         <View style={styles.detail}>
//           <Text style={styles.name}>{item.name}</Text>
//           <Text style={styles.character}>as {item.character}</Text>
//         </View>
//       </View>
//     );
//   };
//   return (
//     <FlatList
//       keyExtractor={item => item.cast_id.toString()}
//       data={data}
//       renderItem={renderItem}
//     />
//   );
// };

const TabCast = ({ detail }) => {
  return (
    <ScrollView>
      {detail.casts.cast.map(cast => (
        <View key={cast.id}>
          <Image
            source={{ uri: `${TMBD_IMG}${cast.profile_path}` }}
            style={styles.profile_pict}
          />
          <Text style={styles.name}>{cast.name}</Text>
          <Text style={styles.character}>as {cast.character}</Text>
        </View>
      ))}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  itemContainer: {
    flexDirection: "row",
    padding: 8
  },
  img: {
    height: 80,
    width: 80,
    alignSelf: "center",
    borderRadius: 40
  },
  detail: {
    flex: 1,
    padding: 16,
    justifyContent: "center"
  },
  name: {
    color: "#fff"
  },
  character: {
    color: "#fff"
  },
  //////////////////////

  profile_pict: {
    width: 75,
    height: 75,
    borderRadius: 35,
    marginTop: 15
  },
  name: {
    color: "white",
    fontSize: 15,
    width: "100%",
    position: "absolute",
    paddingLeft: 100,
    paddingTop: 30
  },
  character: {
    color: "grey",
    fontSize: 15,
    width: "100%",
    position: "absolute",
    paddingLeft: 100,
    paddingTop: 50
  }
});

export default TabCast;

import TabCast from "./TabCasts";
import TabInfo from "./TabInfo";
import TabTrailers from "./TabTrailers";

export { TabCast, TabInfo, TabTrailers };

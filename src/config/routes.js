import React from "react";
import { ScrollView } from "react-native";
import {
  createAppContainer,
  createDrawerNavigator,
  createStackNavigator,
  createBottomTabNavigator,
  DrawerItems
} from "react-navigation";
import Icon from "react-native-vector-icons/Ionicons";

import Movies from "../screens/Movies";
import MovieList from "../screens/MovieListScreen";
import MovieDetails from "../screens/MovieDetails";
import SearchScreen from "../screens/SearchScreen";
import TabMovie from "../screens/TabMovie";
import Carousel from "../screens/Carousel";

const MovieStack = createStackNavigator({
  Movies: {
    screen: Movies
  },
  MovieList: {
    screen: MovieList
  },
  MovieDetails: {
    screen: MovieDetails
  }
});

const SearchStack = createStackNavigator({
  SearchScreen: {
    screen: SearchScreen
  }
});

// const TabStack = createBottomTabNavigator({
//   Home: MovieStack,
//   Calculator: TabMovie
//   //   Carousel: Carousel
// });

const MovieDrawer = createDrawerNavigator(
  {
    Movies: {
      screen: MovieStack,
      navigationOptions: () => {
        return {
          title: "Movies",
          drawerIcon: <Icon name="md-film" color="#ccc" size={30} />
        };
      }
    },
    Search: {
      screen: SearchStack,
      navigationOptions: () => {
        return {
          title: "Search",
          drawerIcon: <Icon name="md-search" color="#ccc" size={30} />
        };
      }
    }
    // TabNav: {
    //   screen: TabStack
    // }
  },
  {
    drawerBackgroundColor: "#000",
    style: { justifyContent: "center" },
    contentComponent: props => <DrawerItems {...props} />,
    contentOptions: {
      labelStyle: {
        color: "white",
        fontSize: 25
      }
    }
  }
);

export default createAppContainer(MovieDrawer);

import {
  GET_MOVIES_DETAIL,
  GET_MOVIES_DETAIL_FAILED,
  GET_MOVIES_DETAIL_SUCCESS
} from "../actions/types";

const initialState = {
  loading: false
};

const detailReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_MOVIES_DETAIL:
      return {
        ...state,
        loading: true
      };
    case GET_MOVIES_DETAIL_SUCCESS:
      return {
        data: action.data,
        loading: false
      };
    case GET_MOVIES_DETAIL_FAILED:
      return {
        ...state,
        loading: false,
        error: action.error
      };
    default:
      return state;
  }
};

export default detailReducer;

import {
  SEARCH_MOVIES,
  SEARCH_MOVIES_FAILED,
  SEARCH_MOVIES_SUCCESS,
  CLEAR_MOVIE_LIST
} from "../actions/types";

const initialState = {
  loading: false
};

const searchListReducer = (state = initialState, action) => {
  switch (action.type) {
    case SEARCH_MOVIES:
      return {
        ...state,
        loading: true
      };
    case SEARCH_MOVIES_SUCCESS:
      return {
        ...action.data,
        loading: false
      };
    case SEARCH_MOVIES_FAILED:
      return {
        ...state,
        loading: false,
        error: action.error
      };
    case CLEAR_MOVIE_LIST:
      return {
        ...state,
        data: null
      };
    default:
      return state;
  }
};

export default searchListReducer;

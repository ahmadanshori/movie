import {
  GET_MOVIES_LIST,
  GET_MOVIES_LIST_FAILED,
  GET_MOVIES_LIST_SUCCESS,
  CLEAR_SEARCH_PAGE
} from "../actions/types";

const initialState = {
  loading: false
};

const movieListReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_MOVIES_LIST:
      return {
        ...state,
        loading: true
      };
    case GET_MOVIES_LIST_SUCCESS:
      const prevResults = state.results || [];
      return {
        ...action.data,
        results: [...prevResults, ...action.data.results],
        loading: false
      };
    case GET_MOVIES_LIST_FAILED:
      return {
        ...state,
        loading: false,
        error: action.error
      };
    case CLEAR_SEARCH_PAGE:
      return initialState;
    default:
      return state;
  }
};

export default movieListReducer;

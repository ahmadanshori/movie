import {
  GET_POPULAR_MOVIES,
  GET_POPULAR_MOVIES_FAILED,
  GET_POPULAR_MOVIES_SUCCESS
} from "../actions/types";

const initialState = {
  loading: false,
  results: []
};

const popularReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_POPULAR_MOVIES:
      return {
        ...state,
        loading: true
      };
    case GET_POPULAR_MOVIES_SUCCESS:
      console.log(action);
      return {
        ...state,
        results: action.data,
        loading: false
      };
    case GET_POPULAR_MOVIES_FAILED:
      return {
        ...state,
        loading: false,
        error: action.error
      };
    default:
      return state;
  }
};

export default popularReducer;

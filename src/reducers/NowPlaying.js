import {
  GET_NOWPLAYING_MOVIES,
  GET_NOWPLAYING_MOVIES_FAILED,
  GET_NOWPLAYING_MOVIES_SUCCESS
} from "../actions/types";

const initialState = {
  loading: false,
  results: []
};

const nowPlayingReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_NOWPLAYING_MOVIES:
      return {
        ...state,
        loading: true
      };
    case GET_NOWPLAYING_MOVIES_SUCCESS:
      return {
        ...state,
        results: action.data,
        loading: false
      };
    case GET_NOWPLAYING_MOVIES_FAILED:
      return {
        ...state,
        loading: false,
        error: action.error
      };
    default:
      return state;
  }
};

export default nowPlayingReducer;

import { combineReducers } from "redux";

import nowPlayingReducer from "./NowPlaying";
import movieListReducer from "./MovieList";
import popularReducer from "./Popular";
import searchListReducer from "./SearchList";
import detailReducer from "./MovieDetail";

export default combineReducers({
  nowPlaying: nowPlayingReducer,
  popular: popularReducer,
  movieList: movieListReducer,
  searchList: searchListReducer,
  movieDetail: detailReducer
});

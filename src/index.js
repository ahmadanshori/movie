import React, { Component } from "react";
import { Provider } from "react-redux";
import configureStore from "./config/configureStore";
import EStyleSheet from "react-native-extended-stylesheet";

import Navigator from "./config/routes";

EStyleSheet.build({
  $primaryBlack: "#000000"

  // $outline: 1
});
class App extends Component {
  render() {
    return (
      <Provider store={configureStore()}>
        <Navigator />
      </Provider>
    );
  }
}
export default App;
// export default () => <Navigator />;

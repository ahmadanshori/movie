import {
  GET_MOVIES_DETAIL,
  GET_MOVIES_LIST,
  GET_POPULAR_MOVIES,
  GET_NOWPLAYING_MOVIES,
  SEARCH_MOVIES,
  CLEAR_SEARCH_PAGE,
  GET_MOVIE_BY_QUERY
} from "./types";

export const getList = ({ category, page }) => ({
  type: GET_MOVIES_LIST,
  category,
  page
});

export const getDetail = id => ({
  type: GET_MOVIES_DETAIL,
  id
});

export const getPopular = () => ({
  type: GET_POPULAR_MOVIES
});

export const getNowPlaying = () => ({
  type: GET_NOWPLAYING_MOVIES
});

export const searchMovie = query => {
  return {
    type: SEARCH_MOVIES,
    query
  };
};

export const getMovieByQuery = (keyword, page) => ({
  type: GET_MOVIE_BY_QUERY,
  keyword,
  page
});

export const clearMovieList = () => ({
  type: CLEAR_SEARCH_PAGE
});
